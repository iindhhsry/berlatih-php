<?php

require_once('animal.php');

class Frog extends Animal
{
    public $name;
    public $legs = 2;
    public $cold_blooded = 0;
    public $jump;


    public function jump()
    {
        echo " Jump: Hop Hop";
    }
}
